#!/bin/bash

DIR=` ls -d $HOME/Videos/ 2>/dev/null | dmenu -p "Videos>" -l 10 `
DIRA=` echo -e "$DIR" | awk ' {print $1} ' `
CMD=` echo -e "$DIR" | awk ' { print $2 } ' `


while [ -n $DIR ]
do
	DIR=` ls -d $HOME/Videos/ 2>/dev/null | dmenu -p "Videos>" -l 10 `
	DIRA=` echo -e "$DIR" | awk ' {print $1} ' `
	CMD=` echo -e "$DIR" | awk ' { print $2 } ' `
done

mpv --fs $DIRA

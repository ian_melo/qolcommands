#!/bin/sh

DIR=` ls -d $HOME/Pictures/Wallpapers/*/ | dmenu -p "DIR: " -l 10 `

while [ -n $DIR ]
do
	DIR=` ls -d $DIR*/ 2>/dev/null | dmenu -p "DIR: " -l 10 `
	DIRA=$DIR
done

sxiv -t "$DIRA"

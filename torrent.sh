#!/bin/sh

mkdir -p $HOME/.config/Torraut
TADIR=$HOME/.config/Torraut
touch $TADIR/custom_folders
CF=$TADIR/custom_folders
URL=$1

#TOR=` ls -1 Downloads| dmenu -l 20 `
#FNAME=` transmission-show $TOR | transmission-show Downloads/1220482.torrent | awk ' /^Name: / {$1=""; print $0} ' `

pgrep -x transmission-da || transmission-daemon

DC=` cat $CF `
DIR=` echo -e "$DC" | dmenu -p "Folder: " -l 10`
[ -z $DIR ] exit 1

grep $DIR $CF || echo "$DIR" >> $CF
DIR=` echo -e "$HOME/$DIR" `
mkdir -p $DIR

notify-send -t 5000 Torr Started! && transmission-remote -ep -D -w $DIR -a $URL

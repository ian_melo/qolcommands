#!/bin/sh 
rm -r $HOME/.cache/wal
WALL="$HOME/.cache/Wallpaper/wall.jpg"
convert "$1" -resize 1366x768! "$WALL" &&
wal -i "$WALL" &&
cat ~/.cache/wal/colors.Xresources > ~/.Xresources &&
kill -HUP $(pgrep dwm)

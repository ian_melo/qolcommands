#!/bin/sh

REMDISK=$(lsblk -o label | sed '1,6d' | dmenu)
fuser -Mmk /run/media/ianm/$REMDISK
udiskie-umount -d /run/media/ianm/$REMDISK

#!/bin/sh

pgrep transmission && echo "Transmission is Running!" | dmenu

CMD=` printf "Shutdown\nReboot" | dmenu `
case $CMD in
	Shutdown) shutdown now;;
	Reboot) reboot;;
esac

#!/bin/sh

W="$HOME/Pictures/Wallpapers"

C=` echo -e "Anime\nSOTC" | dmenu -p "Wallpaper Style: " `
case $C in
	"Anime") D=` ls -d ${W}/Anime/*/ | dmenu -l 10 `;;
	"SOTC") D="${W}/SOTC_ICO";;
esac
sxiv -ft "$D"
